import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.cluster import KMeans, SpectralClustering

df = pd.read_csv('data.csv', sep=';')
df = df[['steps','energy_in','energy_out','heart_rate','stress_level','anxiety_level']]

# drop rows that contain NaN
df = df.dropna()

# Pandas DataFrame -> Numpy array
df = df.values

# normalization by each column
df = df / df.max(axis=0)

# `random_state` is a seed which determines how sklearn shuffles data
df_train, df_test = train_test_split(df, test_size=0.05, random_state=42)

#model = KMeans(n_clusters=2, n_init=20).fit(df_train)
model = SpectralClustering(n_clusters=2, affinity='nearest_neighbors', assign_labels='kmeans')

labels = model.fit_predict(df_test)

fig = plt.figure(figsize=(10, 7))
ax = plt.axes(projection="3d")
ax.scatter3D(df_test[:, -3], df_test[:, -2], df_test[:, -1], c=labels, s=10, cmap='viridis', alpha=0.2)
plt.show()